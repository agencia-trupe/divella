-- phpMyAdmin SQL Dump
-- version 3.3.7deb7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Out 01, 2013 as 08:39 AM
-- Versão do Servidor: 5.1.66
-- Versão do PHP: 5.3.3-7+squeeze17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `divella2`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastros`
--

CREATE TABLE IF NOT EXISTS `cadastros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resposta` varchar(45) DEFAULT NULL,
  `nome` varchar(140) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `endereco` varchar(140) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(140) DEFAULT NULL,
  `bairro` varchar(140) DEFAULT NULL,
  `cidade` varchar(140) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `telefone` varchar(17) DEFAULT NULL,
  `email` varchar(140) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `local_compra` varchar(140) DEFAULT NULL,
  `cod_produto_1` varchar(13) DEFAULT NULL,
  `cod_produto_2` varchar(13) DEFAULT NULL,
  `cod_produto_3` varchar(13) DEFAULT NULL,
  `aceite_regulamento` int(11) NOT NULL DEFAULT '0',
  `receber_newsletter` int(11) NOT NULL DEFAULT '0',
  `data_cadastro` datetime DEFAULT NULL,
  `ip_cadastro` varchar(16) DEFAULT NULL,
  `excluido` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cod_produto_1_UNIQUE` (`cod_produto_1`),
  UNIQUE KEY `cod_produto_2_UNIQUE` (`cod_produto_2`),
  UNIQUE KEY `cod_produto_3_UNIQUE` (`cod_produto_3`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `cadastros`
--

INSERT INTO `cadastros` (`id`, `resposta`, `nome`, `cpf`, `endereco`, `numero`, `complemento`, `bairro`, `cidade`, `estado`, `cep`, `telefone`, `email`, `data_nascimento`, `sexo`, `local_compra`, `cod_produto_1`, `cod_produto_2`, `cod_produto_3`, `aceite_regulamento`, `receber_newsletter`, `data_cadastro`, `ip_cadastro`, `excluido`) VALUES
(8, 'divella', 'orlando facioli junor', '145.620.628-19', 'rua sao carlos do pinhal', '152', 'ap 92', 'bela vista', 'sao paulo', 'sp', '01333-000', '(11) 9814-50660', 'orlando@orlandofacioli.com.br', '1969-11-29', 'M', 'Pão de Acúcar Brigadeiro', '1234567891012', '1457852666666', '6616161646466', 1, 1, '2013-09-26 17:38:39', '201.81.180.196', 0),
(9, 'divella', 'orlando facioli junior', '145.620.628-19', 'Rua São Carlos do Pinhal', '152', 'ap92', 'Bela Vista', 'São Paulo', 'SP', '01333-000', '(11) 9814-50660', 'orlando@orlandofacioli.com.br', '1969-11-29', 'M', 'Pao de Acúcar Brigadeiro', '1254446666646', '4644646464646', '6444666412222', 1, 0, '2013-09-27 16:43:33', '201.81.180.196', 0),
(10, 'outros', 'Fernanda Alves de Moraes', '296.362.818-82', 'Travessa Comendador Payao', '67', '', 'Santa Luzia', 'Bragança Paulista', 'SP', '12919-504', '(11) 9921-78220', 'femalves@yahoo.com', '1981-12-16', 'F', '.', '1234567891011', '1110987654321', '2468101214161', 1, 1, '2013-09-27 17:02:07', '172.16.73.102', 0),
(11, 'divella', 'Liliane', '278.819.568-01', 'Rua Cristóvão Diniz', '67', 'apto 1', 'Cerqueira César', 'São Paulo', 'SP', '01426-020', '(11) 9893-22777', 'liliane@lapastina.com', '1980-07-07', 'F', 'pão de açúcar oscar freire', '1345678901235', '1234567890123', '1234567890123', 1, 1, '2013-09-27 18:47:10', '172.16.101.78', 0),
(12, 'divella', 'CLAUDETE MUTTI MORAES ALVES', '296.362.818-82', 'Travessa Comendador Payao', '67', '', 'Santa Luzia', 'Bragança Paulista', 'SP', '12919-504', '(11) 4033-1449', 'femalves@gmail.com', '1997-03-01', 'F', '.', '2468517942105', '1213141516171', '8192021222324', 1, 0, '2013-09-30 16:34:56', '172.16.73.102', 0),
(13, 'divella', 'maria aparecida', '297.983.238-31', 'Avenida Santa Catarina', '915', '', 'Vila Mascote', 'São Paulo', 'SP', '04378-300', '(11) 1111-11111', 'maparecida@gmail.com', '1999-05-10', 'F', '.', '1111111111111', '2222222222222', '1111111111111', 1, 0, '2013-09-30 16:44:27', '172.16.73.102', 0),
(14, 'divella', 'Sergiane Nunes', '297.983.238-31', 'Travessa Comendador Payao', '67', '', 'Santa Luzia', 'Bragança Paulista', 'SP', '12919-504', '(11) 1111-11111', 'eunaosei@hormail.com', '2000-09-04', 'F', '.', '2222222211111', '3333333111111', '5555555511111', 1, 1, '2013-09-30 16:58:12', '172.16.73.102', 0),
(15, 'divella', 'Testes dois', '016.454.518-28', 'Avenida Presidente Wilson', '1866', '', 'Mooca', 'São Paulo', 'SP', '03107-001', '(11) 1111-11111', 'eunaotenho@com.br', '2013-01-01', 'F', '.', '5566886611799', '5166466978161', '8318415634969', 1, 0, '2013-09-30 17:03:25', '172.16.73.102', 0),
(16, 'divella', 'Bruno Monteiro', '331.847.568-82', 'Rua Oswaldo Diniz', '74', '', 'Jardim Satélite', 'São Paulo', 'SP', '04815-330', '(11) 5666-6117', 'supercharger.v8@gmail.com', '1986-12-06', 'M', 'Não lembro', '8888878888888', '7777787777777', '6666696666666', 1, 0, '2013-10-01 08:30:06', '177.148.135.26', 0);
