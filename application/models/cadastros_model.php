<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = "cadastros";

        $this->dados = array();
        $this->dados_tratados = array();
	}

  function pegarTodos($order_campo = 'id', $order = 'ASC'){
    return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('excluido' => 0))->result();
  }

  function numeroResultados(){
    return $this->db->get_where($this->tabela, array('excluido' => 0))->num_rows();
  }

  function pegarPorId($id){
    $qry = $this->db->get_where($this->tabela, array('id' => $id, 'excluido' => 0))->result();
    if(isset($qry[0]))
      return $qry[0];
    else
      return FALSE;
  }

  function pegarPorSlug($slug){
    $qry = $this->db->get_where($this->tabela, array('slug' => $slug, 'excluido' => 0))->result();
    if(isset($qry[0]) && $qry[0])
      return $qry[0];
    else
      return false;
  }

  function pegarPaginado($por_pagina, $inicio, $order_campo = 'id', $order = 'DESC'){
    return $this->db->order_by($order_campo, $order)->get_where($this->tabela, array('excluido' => 0), $por_pagina, $inicio)->result();
  }

  function excluir($id){
    if($this->pegarPorId($id) !== FALSE){
      //return $this->db->where('id', $id)->delete($this->tabela);
      return $this->db->where('id', $id)->set('excluido', 1)->update($this->tabela);
    }
  }
}
