<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_model extends MY_Model {

	function __construct(){
		parent::__construct();

/*
CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) NOT NULL,
  `texto` text NOT NULL,
  `data` date NOT NULL,
  `id_blog_categorias` int(10) unsigned NOT NULL,
  `slug` varchar(140) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE `blog_categorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) NOT NULL,
  `slug` varchar(140) NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8$$

CREATE TABLE `blog_comentarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_blog` int(10) unsigned NOT NULL,
  `nome` varchar(140) NOT NULL,
  `email` varchar(140) NOT NULL,
  `comentario` text NOT NULL,
  `data` date NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

CREATE TABLE `blog_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(140) NOT NULL,
  `id_blog` int(10) unsigned NOT NULL,
  `legenda` varchar(140) DEFAULT NULL,
  `ordem` varchar(45) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8$$

*/

		$this->tabela = 'blog';

		$this->dados = array('titulo', 'texto', 'data', 'id_blog_categorias', 'slug');
		$this->dados_tratados = array(
			'data' => formataData($this->input->post('data'), 'br2mysql')
		);
	}

	function inserir(){

        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		foreach($this->dados as $k => $v){
			if(array_key_exists($v, $this->dados_tratados))
				$this->db->set($v, $this->dados_tratados[$v]);
			elseif($this->input->post($v) !== FALSE)
				$this->db->set($v, $this->input->post($v));
		}
		$this->db->set('slug', $slug);
		return $this->db->insert($this->tabela);
	}

	function alterar($id){

        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			$this->db->set('slug', $slug);
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function excluir($id){
		if($this->pegarPorId($id) !== FALSE){
			$this->db->delete('blog_comentarios', array('id_blog' => $id));
			$imgs = $this->imagens($id);
			foreach($imgs as $k => $v){
				@unlink('_imgs/blog/'.$v->imagem);
				@unlink('_imgs/blog/thumbs/'.$v->imagem);
			}
			$this->db->delete('blog_imagens', array('id_blog' => $id));
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}	

	function categorias($id = false){
		if($id){
			$qry = $this->db->get_where('blog_categorias', array('id' => $id))->result();
			if(isset($qry[0]))
				return $qry[0];
			else
				return false;
		}else{
			return $this->db->order_by('ordem', 'ASC')->get('blog_categorias')->result();
		}
	}

	function categoriasPorSlug($slug = false){
		if($slug){
			$qry = $this->db->get_where('blog_categorias', array('slug' => $slug))->result();
			if(isset($qry[0]))
				return $qry[0];
			else
				return false;
		}else{
			return $this->db->get('blog_categorias')->result();
		}
	}

	function pegarPaginadoPorCategoria($por_pagina, $inicio, $order_campo = 'data', $order = 'desc', $categoria){
		return $this->db->order_by($order_campo, $order)->where('id_blog_categorias', $categoria)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function pegarPaginadoPorAno($por_pagina, $inicio, $order_campo = 'data', $order = 'desc', $ano){
		return $this->db->order_by($order_campo, $order)->where('YEAR(data)', $ano)->get($this->tabela, $por_pagina, $inicio)->result();	
	}

	function navegacao_categorias($categoria = ''){
		$retorno = array();
		$categorias = $this->categorias();
		if($categorias){
			foreach ($categorias as $key => $value) {
				$num_posts = $this->db->get_where('blog', array('id_blog_categorias' => $value->id))->num_rows();
				if($value->slug == $categoria)
					$retorno[] = "<a href='home/categoria/".$value->slug."' title='".$value->titulo."' class='subativo'>".$value->titulo."</a>"; 
				else
					$retorno[] = "<a href='home/categoria/".$value->slug."' title='".$value->titulo."'>".$value->titulo."</a>"; 
			}
		}else{
			$retorno = false;
		}
		return $retorno;
	}

	function navegacao_anos($ano = ''){
		$retorno = array();
		$anos = array();
		for ($i=date('Y'); $i >= 2012  ; $i--) { 
			$anos[] = $i;
		}
		foreach ($anos as $key => $value) {
			$num_posts = $this->db->get_where('blog', array('YEAR(data)' => $value))->num_rows();
			if($value == $ano)
				$retorno[] = "<a href='home/ano/".$value."' title='Posts de ".$value."' class='subativo'>&raquo; ".$value." [".$num_posts."]</a>"; 
			else
				$retorno[] = "<a href='home/ano/".$value."' title='Posts de ".$value."'>&raquo; ".$value." [".$num_posts."]</a>"; 
		}
		return $retorno;
	}

	function inserirCategoria(){
        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where('blog_categorias', array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where('blog_categorias', array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		return $this->db->set('titulo', $this->input->post('titulo'))
				 		->set('slug', $slug)
				 		->insert('blog_categorias');
	}

	function alterarCategoria($id){
        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where('blog_categorias', array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where('blog_categorias', array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		return $this->db->set('titulo', $this->input->post('titulo'))
				 		->set('slug', $slug)
				 		->where('id', $id)
				 		->update('blog_categorias');
	}

	function excluirCategoria($id){
		$this->db->query("UPDATE blog SET id_blog_categorias = '9999' WHERE id_blog_categorias = '$id'");
		return $this->db->delete('blog_categorias', array('id' => $id));
	}

	function imagens($id = false){
		if($id){
			return $this->db->order_by('ordem', 'asc')->get_where('blog_imagens', array('id_blog' => $id))->result();
		}else{
			return $this->db->order_by('ordem', 'asc')->get('blog_imagens')->result();
		}
	}

	function inserirImagem($id_post){
		$imagem = $this->sobeImagem();
		$this->db->set('legenda', $this->input->post('legenda'));
		$this->db->set('id_blog', $id_post);

		if($imagem)
			$this->db->set('imagem', $imagem);		

		return $this->db->insert('blog_imagens');
	}

	function alterarImagem($id_imagem){
		$imagem = $this->sobeImagem();
		$this->db->set('legenda', $this->input->post('legenda'));
		
		if($imagem)
			$this->db->set('imagem', $imagem);		

		return $this->db->where('id', $id_imagem)->update('blog_imagens');	
	}

	function excluirImagem($id_imagem){
		$atual = $this->db->get_where('blog_imagens', array('id' => $id_imagem))->result();
		@unlink('_imgs/blog/'.$atual->imagem);
		@unlink('_imgs/blog/thumbs/'.$atual->imagem);
		return $this->db->delete('blog_imagens', array('id' => $id_imagem));
	}

	function num_comentarios($id_post){
		$retorno = $this->db->get_where('blog_comentarios', array('id_blog' => $id_post))->num_rows();
		if($retorno > 1)
			return $retorno . " comentários";
		elseif($retorno == 1)
			return $retorno . " comentário";
		else
			return "Nenhum comentário. Seja o primeiro!";
	}

	function comentarios($id_post){
		return $this->db->order_by('datetime', 'desc')->get_where('blog_comentarios', array('id_blog' => $id_post))->result();
	}

	function inserirComentario($id_post, $nome, $email, $comentario){
		return $this->db->set('id_blog', $id_post)
						->set('nome', $nome)
						->set('email', $email)
						->set('comentario', $comentario)
						->set('data', date('Y-m-d'))
						->set('datetime', date('Y-m-d H:i:s'))
						->insert('blog_comentarios');
	}

	function sobeImagem(){

		$this->load->library('upload');

		$dir = '_imgs/blog/';
		$campo = 'userfile';

		$uploadconfig = array(
		  'upload_path' => $dir,
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if($_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($dir.$arquivo['file_name'] , $dir.$filename);
		        
		          $this->image_moo
		                  ->load($dir.$filename)
		                  ->resize(800, 800)
		                  ->save($dir.$filename, TRUE)
		                  ->resize_crop(130, 130)
		                  ->save($dir.'thumbs/'.$filename, TRUE);
		        
		        return $filename;
		    }
		}else{
		    return false;
		}		
	}

}