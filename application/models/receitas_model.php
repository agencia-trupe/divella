<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receitas_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'receitas';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('titulo', 'slug', 'imagem', 'ingredientes', 'preparo');
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem(),
			'slug' => url_title($this->input->post('titulo'))
		);
	}

	function pegarTodos($order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}
	function pegarPaginado($por_pagina, $inicio, $order_campo = 'ordem', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function sobeImagem(){
		$this->load->library('upload');

		$campo = 'userfile';

		$uploadconfig = array(
		  'upload_path' => '_imgs/receitas/',
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($uploadconfig['upload_path'].$arquivo['file_name'] , $uploadconfig['upload_path'].$filename);

		        $this->image_moo
		             ->load($uploadconfig['upload_path'].$filename)
		             ->resize(260,800)
		             ->save($uploadconfig['upload_path'].$filename, TRUE)
		             ->resize_crop(165,102)
		             ->save($uploadconfig['upload_path'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}
}