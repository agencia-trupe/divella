<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Divella</title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:title" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content=""/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <?CSS(array('reset', 'teaser'))?>


  <?if(ENVIRONMENT == 'development'):?>

    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min'))?>

  <?else:?>

    <?JS(array('modernizr-2.0.6.min'))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>

    <div class="centro">
        <a href="http://www.lapastina.com.br/" title="La Pastina" id="link-lapastina"></a>
        <img src="" data-imagem="_imgs/layout/teaser-conteudo.png" id="load-imagem">
    </div>

    <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
        <script>
            window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
            Modernizr.load({
                load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
            });
        </script>
    <?endif;?>

    <?JS(array('front'))?>

</body>
</html>
