<div class="agradecimento">
	<?php if ($this->session->flashdata('cadastro_finalizado') == 'N'): ?>

		<div class="concluido">
			Houve um erro ao efetuar o cadastro<br>
			Por favor, tente novamente.
		</div>

		<a href="cadastro" class="nav-link w133 centralizado" title="Voltar">VOLTAR</a>

	<?php else: ?>
		<div id="aviao"><img src="_imgs/layout/aviaozinho.png"></div>

		<div class="concluido">
			Dados enviados com sucesso.<br>
			Boa sorte!
		</div>


	<?php endif ?>
</div>