<div class="regulamento">
	<p>1) Promoção válida em todo território nacional, no período de 14/10/2013 à 14/12/2013, com apuração em 20/12/2013 realizada pela CLP Prestação de Serviçoes Administrativos Sociedade Simples Ltda. (Mandatária), com sede na Rua Silvestre Vasconcelos Calmon, 190 – Cj. 604 – Guarulhos/SP, inscrita no CNPJ nº 02.780.640/0001-97 e Real Comercial Ltda. (Aderente), com sede na Rua Silvestre Vasconcelos Calmon, 190 – Cj. 604 – Guarulhos/SP, inscrita no CNPJ nº 02.780.640/0001-97.</p>

	<p>2) Produtos em promoção: Produtos da marca Divella (massas, biscoitos, arroz, polenta, couscous, pomodori pelati e passata e sêmola).</p>

	<p>3) Qualquer pessoa física maior de 18 anos, residente e domiciliada em território nacional, poderá participar da presente promoção, promovida pela marca Divella, no período de 14/10/2013 à 14/12/2013.</p>

	<p>4) Para participar da promoção <strong>“Viaggia che ti fa bene”</strong>, o consumidor que adquirir 03 (três) produtos Divella participantes da promoção, na mesma nota fiscal no período de 14/10/2013 a 14/12/2013, terá direito a um cupom nos postos de trocas da promoção ou se cadastrar no hotsite da promoção</p>

	<p><strong>Participação pelos Pontos de Trocas:</strong></p>

	<p>5) O consumidor que adquirir 03 (três) produtos Divella participantes da promoção, na mesma nota fiscal, poderá retirar no posto de trocas da promoção um cupom mediante a apresentação do comprovante de compra, que deverá ser preenchido com seus dados pessoais (Nome completo, CPF, Idade, Endereço completo, CEP, telefone, e-mail, sexo, código de barras do produto, nota fiscal da compra, loja onde comprou) e responder à pergunta: <strong>Qual é a massa de grano duro 100% Italiana no Brasil?.<br>Resposta: (   ) Divella		(   ) Outros</strong></p>

	<p>5.1) Após o preenchimento do(s) cupom(ns), o participante deverá colocar o(s) cupom(ns) em urna no próprio balcão de trocas, conforme período de participação descrito no item 3.1, sendo que os cupons entregues após esta data não serão válidos para a participação no concurso.</p>

	<p>5.2) Serão invalidados os cupons que não estiverem legíveis, com dados pessoais incompletos, sem a resposta a pergunta da promoção, sem a identificação do nº. do RG e CPF e sem identificação do endereço completo e telefone.</p>

	<p>5.3) Os postos de trocas e urnas da promoção estarão situados nos endereços abaixo:
	Empório EAT localizado na Avenida Doutor Cardoso de Melo, 1191 – Vila Olímpia – São Paulo/SP
	Makro Vl Maria localizado na Avenida Morvan Dias Figueiredo, 3231 - Vila Maria -  São Paulo/SP
	Mambo localizado na Rua Deputado Lacerda Franco, 553 - Pinheiros – São Paulo/SP
	Sacolão São Jorge localizado na Avenida Pompéia, 499 -  Vila Pompéia – São Paulo/SP
	Sonda localizado na Avenida Francisco Matarazzo, 892 – Agua Branca - São Paulo/SP
	Sonda localizado na Rua Carlos Vicari, 155/197 -  Agua Branca -  São Paulo/SP
	Super Ville localizado na Rua Coriolano, 1071 -  Vila Romana – São Paulo/SP</p>

	<p><strong>Participação pela Internet</strong></p>

	<p>6) O consumidor que adquirir 03 (três) produtos Divella participante da promoção, na mesma nota fiscal, poderá se inscrever na promoção acessando o site www.divella.com.br, no período de 10h00 do dia 14/10/2013 a 23h59 do dia 14/12/2013 com seus dados pessoais (Nome completo, CPF, Idade, Endereço completo, CEP, telefone, e-mail, sexo, código de barras do produto, nota fiscal da compra, loja onde comprou) e responder à pergunta: <strong>Qual é a massa de grano duro 100% Italiana no Brasil?.<br>Resposta: (   ) Divella		(   ) Outros</strong></p>

	<p>6.1) Após a inscrição, os participantes não precisarão adotar nenhum procedimento, desta forma será emitido um cupom pela empresa promotora e colocado em urna para a apuração.
	Obs.:
	O participante receberá a confirmação de sua inscrição na finalização do cadastro com a seguinte mensagem “cadastro realizado com sucesso”.
	Os cupons impressos da internet serão iguais aos cupons distribuídos nas lojas, ou seja com o mesmo formato, tamanho, cor e papel.</p>

	<p>7) O participante inscrito nesta Promoção deverá armazenar as embalagens e o(s) cupom(ns)/nota(s) fiscal(is) referente(s) à aquisição dos produtos participantes adquiridos e cadastrados durante o período de participação até o término do período de divulgação do contemplado, sendo que, o contemplado deverá, após a apuração, apresentar à Empresa Promotora o(s) referido(s) documento(s), dentro do período de participação correspondente, para validação da condição de contemplado, sob pena de desclassificação. Nessa hipótese, o valor integral do prêmio será recolhido aos cofres da União, como prêmio prescrito.</p>

	<p>8) Não terão validade inscrições que não preencherem as condições básicas do concurso e que impossibilitarem a verificação de sua autenticidade.</p>

	<p>9) Não poderão participar do concurso funcionários da CLP Prestação de Serviços Administrativos Sociedade Simples Ltda. e Real Comercial Ltda. O cumprimento desta cláusula será de responsabilidade da empresa promotora, através de consulta ao banco de dados de funcionários no momento da apuração.</p>

	<p>10) Não poderão participar da promoção, menores de 18 anos de idade.</p>

	<p>11) Os prêmios são intransferíveis e não poderão ser convertidos em dinheiro, nem ser trocados por outro(s) produto(s).</p>

	<p>12) Forma de apuração: Todos os cupons impressos pela empresa promotora serão encaminhados para o local da apuração e colocados em uma única urna. Dessa urna serão retirados, aleatoriamente, tantos cupons quantos forem necessários até que se encontre um cupom devidamente preenchido e com a resposta certa em igualdade com o número de prêmios a distribuir na apuração.</p>

	<p>13) Premiação:
	01 prêmio: 01 (uma) Viagem para São Paulo/Roma/São Paulo de 05 dias e 04 noites para o contemplado e 01 acompanhante, constituído de: passagem aérea de ida/volta, hospedagem em hotel categoria turística, com café da manhã, traslados (residência/aeroporto/hotel/aeroporto/residência) e seguro viagem.
	Prêmio no valor de R$ 10.000,00</p>

	<p>13.1) Observações:
	- O período de fruição da viagem será de 180 (cento e oitenta) dias, contados da assinatura da carta compromisso, deverá ser utilizada em baixa temporada e deverá ser agendada até 30 dias antes da viagem.
	- A empresa não se responsabilizará pelas despesas pessoais realizadas pelo contemplado durante a viagem, tais como: despesas referentes à lavanderia, dispêndios pessoais (aquisição de bens em tour de compras, se for o caso), artigos de toucador, exceto os fornecidos pelo hotel em que ficará hospedado, telefonemas internacionais e locais, souvenires, consumo de itens de frigobar, excesso de bagagem ou seja, quaisquer outras despesas pessoais não previstas no pacote turístico serão de responsabilidade do contemplado.
	- Os documentos pessoais do contemplado como passaporte e vistos necessários para a realização da viagem será de responsabilidade do contemplado.
	- A obtenção de passaporte devidamente válido, juntamente com o respectivo visto que viabilize a entrada do Contemplado e de seu Acompanhante em território estrangeiro será de inteira responsabilidade do Contemplado e de seu Acompanhante, devendo os mesmos arcar com todo e qualquer custo relacionado à obtenção de tais documentos, não podendo sua falta justificar qualquer dificuldade ou impedimento de participar da viagem. No caso de não obtenção/apresentação da documentação necessária à fruição do prêmio ou qualquer fator que impeça o usufruto do prêmio no período determinado para a viagem implicará na perda do direito ao prêmio pelo contemplado.
	- A empresa não assume qualquer responsabilidade por eventuais acidentes ou ocorrências resultantes de força maior que fujam totalmente de seu controle e que venham a causar danos materiais ou físicos ao contemplado e seu acompanhante durante a viagem.
	- O contemplado receberá, no endereço fornecido em cadastro, ou na Administração da empresa, de forma gratuita e sem ônus, uma Carta Compromisso de entrega do prêmio que detalhará as informações para recebimento do pacote turístico, em até 30 dias corridos, contados da data de apuração do resultado. No momento da entrega da Carta Compromisso, contemplado deverá assinar um Termo de Quitação e Entrega de Prêmio e entregar uma cópia simples do seu RG e CPF/MF à empresa promotora.
	- A empresa não se responsabilizará por furtos/roubos de bens de propriedade do contemplado que eventualmente ocorram durante a viagem, sendo que o vínculo da empresa promotora com o contemplado se encerrará a partir do momento da entrega da Carta Compromisso.
	- Caso o contemplado deseje incluir algum passeio com extensão da viagem alem dos inclusos no pacote e/ou altere o período de baixa temporada para alta, e/ou estenda a quantidade de dias/noites, deverá arcar com as diferenças de valores.
	- Caso o contemplado deseje alterar alguma informação após o fechamento do pacote, o mesmo deverá arcar com as diferenças de valores.</p>

	<p>14) Data da apuração: 20/12/2013 para cupons gerados de 14/10/2013 à 14/12/2013..</p>

	<p>15) Local da apuração: Escritório Administrativo á Avenida Presidente Wilson, 1786 e 1866 - São Paulo/SP, na data prevista, às 15:00 horas, com livre acesso aos interessados.</p>

	<p>16) Exibição dos prêmios: Devido a natureza do prêmio o mesmo não será exibido. A empresa comprovará a propriedade dos prêmios até 8 (oito) dias antes da data marcada para realização do concurso de acordo com o Decreto 70951/72 – Art 15 – parágrafo 1º.</p>

	<p>17) Entrega dos prêmios: Escritório Administrativo á Avenida Presidente Wilson, 1786 e 1866 - São Paulo/SP ou ainda no domicilio dos contemplados até 30 dias a contar da data da apuração de acordo com o Decreto 70951/72 – Art 5º.
	Os prêmios distribuídos deverão ser livres e desembaraçados de qualquer ônus para os contemplados.
	A viagem será disponibilizada ao contemplado, no prazo estipulado acima, através de carta compromisso, que conterá todas as informações pertinentes à realização da viagem de acordo com anexo VII da Portaria 41 de 19/02/08.</p>

	<p>18) Conforme o disposto no art. 70, inciso 1º, “b”, da Lei nº. 11.196, de 21/11/05, a instituição promotora recolherá 20% de IRF sobre o valor dos prêmios, até o 3º dia útil subseqüente ao decêndio de ocorrência dos fatos geradores, através de DARF, recolhida na rede bancária, com o código 0916.</p>

	<p>19) Prescrição do direito aos prêmios: 180 (cento e oitenta) dias após a data de cada apuração e os prêmios ganhos e não reclamados reverterão como Renda da União de acordo com o Art. 6º do Decreto 70951/72.</p>

	<p>20) O regulamento completo da promoção estará disponibilizado na sede da empresa e no site www.divella.com.br. Os contemplados serão avisados através de telefone e/ou telegrama e, desde já, autorizam a utilização de seu nome, imagem e som de voz, na divulgação do resultado da promoção sem qualquer ônus para a empresa promotora pelo período de até 01 ano.</p>

	<p>21) As dúvidas e controvérsias oriundas de reclamações dos consumidores participantes da promoção deverão ser preliminarmente dirimidas pelos seus respectivos organizadores e, posteriormente, submetidas à CAIXA/CEPCO.
	O PROCON local, bem como os órgãos conveniados, em cada jurisdição receberão as reclamações devidamente fundamentadas, dos consumidores participantes.</p>

	<p><strong>22) Certificado Autorização Caixa nº. 6-1525/2013 – Distribuição Gratuita</strong></p>

</div>

<?php if(!$this->input->is_ajax_request()): ?>
	<a href="cadastro" class="nav-link centralizado m-top w310" title="Li o Regulamento e quero participar">Li o Regulamento e quero participar</a>
<?php endif; ?>