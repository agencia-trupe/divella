<?php if ($receitas): ?>
	<div class="lista-receitas">

		<?php foreach ($receitas as $key => $value): ?>
			<a href="receitas/<?=$value->slug?>" title="<?=$value->titulo?>">
				<img src="_imgs/receitas/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
				<?=$value->titulo?>
			</a>
		<?php endforeach ?>

	</div>
<?php endif ?>