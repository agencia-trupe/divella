<form action="cadastro/enviar" method="post" id="form-cadastro">

	<div class="box">
		<span class="amarelo menor">RESPONDA A PERGUNTA</span>
		<h3>QUAL É A MASSA GRANO DURO 100% ITALIANA NO BRASIL?</h3>
		<div class="resposta">
			<label><input type="radio" name="resposta" required id="resposta-1" value="divella" <?if($this->session->flashdata('resposta') == 'divella') echo " checked"?>>DIVELLA</label>
			<label><input type="radio" name="resposta" required id="resposta-2" value="outros" <?if($this->session->flashdata('resposta') == 'outros') echo " checked"?>>OUTROS</label>
		</div>
	</div>

	<div class="box">
		<span class="amarelo menor mb">PREENCHA COM SEUS DADOS</span><br>
		<label class="mt">NOME <input type="text" name="nome" required id="input-nome" class="w293" value="<?=$this->session->flashdata('nome')?>"></label>
		<label class='ml15 mt'>CPF <input type="text" name="cpf" maxlength="15" required id="input-cpf" class="w155" value="<?=$this->session->flashdata('cpf')?>"></label>
		<label>ENDEREÇO <input type="text" name="endereco" required id="input-endereco" class="w353" value="<?=$this->session->flashdata('endereco')?>"></label>
		<label class='ml15'>N&ordm; <input type="text" name="numero" required id="input-numero" class="w63" value="<?=$this->session->flashdata('numero')?>"></label>
		<label>COMPLEMENTO <input type="text" name="complemento" id="input-complemento" class="w50" value="<?=$this->session->flashdata('complemento')?>"></label>
		<label>BAIRRO <input type="text" name="bairro" required id="input-bairro" class="w90" value="<?=$this->session->flashdata('bairro')?>"></label>
		<label>CIDADE <input type="text" name="cidade" required id="input-cidade" class="w88" value="<?=$this->session->flashdata('cidade')?>"></label>
		<label>ESTADO <input type="text" name="estado" maxlength="2" required id="input-estado" class="w15" value="<?=$this->session->flashdata('estado')?>"></label>
		<label>CEP <input type="text" name="cep" maxlength="10" required id="input-cep" class="w70" value="<?=$this->session->flashdata('cep')?>"></label>
		<label>TEL <input type="text" name="telefone" maxlength="17" required id="input-telefone" class="w150" value="<?=$this->session->flashdata('telefone')?>"></label>
		<label>EMAIL <input type="email" name="email" required id="input-email" class="w164" value="<?=$this->session->flashdata('email')?>"></label>
		<label class="multi"> <span>DATA DE<br> NASCIMENTO</span> <input type="text" maxlength="10" name="data_nascimento" required id="input-data_nascimento" class="w84" value="<?=$this->session->flashdata('data_nascimento')?>"></label>
		<span class="label-like">
			SEXO <label><input type="radio" name="sexo" required id="input" value="M" <?if($this->session->flashdata('sexo')=='M') echo" checked"?>>M</label>
				 <label><input type="radio" name="sexo" required id="input" value="F" <?if($this->session->flashdata('sexo')=='F') echo" checked"?>>F</label>
		</span>
		<label class="multi ml15">
			<span>SUPERMERCADO OU ESTABELECIMENTO<br>ONDE COMPROU O PRODUTO</span> <input type="text" name="local_compra" required id="input-local_compra" class="w190" value="<?=$this->session->flashdata('local_compra')?>">
		</label>
		<div class="contem-coluna">
			<div class="caixa-amarela">
				CADASTRE AO LADO OS CÓDIGOS DE BARRA DOS 3 PRODUTOS DIVELLA
			</div>
			<div class="codigos">
				<div class="linha">
					<span class="amarelo">PRODUTO 1</span>
					<input type="text" name="prod_1_input_1" required maxlength="1" id="prod_1_input_1" value="<?=$this->session->flashdata('prod_1_input_1')?>">
					<input type="text" name="prod_1_input_2" required maxlength="1" id="prod_1_input_2" value="<?=$this->session->flashdata('prod_1_input_2')?>">
					<input type="text" name="prod_1_input_3" required maxlength="1" id="prod_1_input_3" value="<?=$this->session->flashdata('prod_1_input_3')?>">
					<input type="text" name="prod_1_input_4" required maxlength="1" id="prod_1_input_4" value="<?=$this->session->flashdata('prod_1_input_4')?>">
					<input type="text" name="prod_1_input_5" required maxlength="1" id="prod_1_input_5" value="<?=$this->session->flashdata('prod_1_input_5')?>">
					<input type="text" name="prod_1_input_6" required maxlength="1" id="prod_1_input_6" value="<?=$this->session->flashdata('prod_1_input_6')?>">
					<input type="text" name="prod_1_input_7" required maxlength="1" id="prod_1_input_7" value="<?=$this->session->flashdata('prod_1_input_7')?>">
					<input type="text" name="prod_1_input_8" required maxlength="1" id="prod_1_input_8" value="<?=$this->session->flashdata('prod_1_input_8')?>">
					<input type="text" name="prod_1_input_9" required maxlength="1" id="prod_1_input_9" value="<?=$this->session->flashdata('prod_1_input_9')?>">
					<input type="text" name="prod_1_input_10" required maxlength="1" id="prod_1_input_10" value="<?=$this->session->flashdata('prod_1_input_10')?>">
					<input type="text" name="prod_1_input_11" required maxlength="1" id="prod_1_input_11" value="<?=$this->session->flashdata('prod_1_input_11')?>">
					<input type="text" name="prod_1_input_12" required maxlength="1" id="prod_1_input_12" value="<?=$this->session->flashdata('prod_1_input_12')?>">
					<input type="text" name="prod_1_input_13" required maxlength="1" id="prod_1_input_13" value="<?=$this->session->flashdata('prod_1_input_13')?>">
				</div>
				<div class="linha">
					<span class="amarelo">PRODUTO 2</span>
					<input type="text" name="prod_2_input_1" required maxlength="1" id="prod_2_input_1" value="<?=$this->session->flashdata('prod_2_input_1')?>">
					<input type="text" name="prod_2_input_2" required maxlength="1" id="prod_2_input_2" value="<?=$this->session->flashdata('prod_2_input_2')?>">
					<input type="text" name="prod_2_input_3" required maxlength="1" id="prod_2_input_3" value="<?=$this->session->flashdata('prod_2_input_3')?>">
					<input type="text" name="prod_2_input_4" required maxlength="1" id="prod_2_input_4" value="<?=$this->session->flashdata('prod_2_input_4')?>">
					<input type="text" name="prod_2_input_5" required maxlength="1" id="prod_2_input_5" value="<?=$this->session->flashdata('prod_2_input_5')?>">
					<input type="text" name="prod_2_input_6" required maxlength="1" id="prod_2_input_6" value="<?=$this->session->flashdata('prod_2_input_6')?>">
					<input type="text" name="prod_2_input_7" required maxlength="1" id="prod_2_input_7" value="<?=$this->session->flashdata('prod_2_input_7')?>">
					<input type="text" name="prod_2_input_8" required maxlength="1" id="prod_2_input_8" value="<?=$this->session->flashdata('prod_2_input_8')?>">
					<input type="text" name="prod_2_input_9" required maxlength="1" id="prod_2_input_9" value="<?=$this->session->flashdata('prod_2_input_9')?>">
					<input type="text" name="prod_2_input_10" required maxlength="1" id="prod_2_input_10" value="<?=$this->session->flashdata('prod_2_input_10')?>">
					<input type="text" name="prod_2_input_11" required maxlength="1" id="prod_2_input_11" value="<?=$this->session->flashdata('prod_2_input_11')?>">
					<input type="text" name="prod_2_input_12" required maxlength="1" id="prod_2_input_12" value="<?=$this->session->flashdata('prod_2_input_12')?>">
					<input type="text" name="prod_2_input_13" required maxlength="1" id="prod_2_input_13" value="<?=$this->session->flashdata('prod_2_input_13')?>">
				</div>
				<div class="linha">
					<span class="amarelo">PRODUTO 3</span>
					<input type="text" name="prod_3_input_1" required maxlength="1" id="prod_3_input_1" value="<?=$this->session->flashdata('prod_3_input_1')?>">
					<input type="text" name="prod_3_input_2" required maxlength="1" id="prod_3_input_2" value="<?=$this->session->flashdata('prod_3_input_2')?>">
					<input type="text" name="prod_3_input_3" required maxlength="1" id="prod_3_input_3" value="<?=$this->session->flashdata('prod_3_input_3')?>">
					<input type="text" name="prod_3_input_4" required maxlength="1" id="prod_3_input_4" value="<?=$this->session->flashdata('prod_3_input_4')?>">
					<input type="text" name="prod_3_input_5" required maxlength="1" id="prod_3_input_5" value="<?=$this->session->flashdata('prod_3_input_5')?>">
					<input type="text" name="prod_3_input_6" required maxlength="1" id="prod_3_input_6" value="<?=$this->session->flashdata('prod_3_input_6')?>">
					<input type="text" name="prod_3_input_7" required maxlength="1" id="prod_3_input_7" value="<?=$this->session->flashdata('prod_3_input_7')?>">
					<input type="text" name="prod_3_input_8" required maxlength="1" id="prod_3_input_8" value="<?=$this->session->flashdata('prod_3_input_8')?>">
					<input type="text" name="prod_3_input_9" required maxlength="1" id="prod_3_input_9" value="<?=$this->session->flashdata('prod_3_input_9')?>">
					<input type="text" name="prod_3_input_10" required maxlength="1" id="prod_3_input_10" value="<?=$this->session->flashdata('prod_3_input_10')?>">
					<input type="text" name="prod_3_input_11" required maxlength="1" id="prod_3_input_11" value="<?=$this->session->flashdata('prod_3_input_11')?>">
					<input type="text" name="prod_3_input_12" required maxlength="1" id="prod_3_input_12" value="<?=$this->session->flashdata('prod_3_input_12')?>">
					<input type="text" name="prod_3_input_13" required maxlength="1" id="prod_3_input_13" value="<?=$this->session->flashdata('prod_3_input_13')?>">
				</div>
			</div>
		</div>
		<div class="box-aviso"><span class="vermelho">IMPORTANTE</span>: GUARDE AS EMBALAGENS DOS 3 PRODUTOS CADASTRADOS E O CUPOM FISCAL,<br>POIS SERÁ NECESSÁRIA A APRESENTAÇÃO NO MOMENTO DA PREMIAÇÃO.</span></div>
	</div>

	<label class="ps"><input type="checkbox" required value="1" name="aceite_regulamento" id="aceite_regulamento" <?if($this->session->flashdata('aceite_regulamento') == 1)echo "checked"?>> Li e aceito o <a href="regulamento" title="Ver o regulamento" class="shadow">regulamento</a>.</label>
	<label class="ps"><input type="checkbox" value="1" name="receber_newsletter" id="receber_newsletter" <?if($this->session->flashdata('receber_newsletter') == 1)echo "checked"?>> Gostaria de receber informações e promoções de Divella.</label>
	<input type="submit" value="Enviar">

</form>

<?php if ($this->session->flashdata('msg_erro')): ?>
	<script defer>alert('<?=$this->session->flashdata('msg_erro')?>');</script>
<?php endif ?>