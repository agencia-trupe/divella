<div class="detalhe-receita">

	<h3><?=$detalhes[0]->titulo?></h3>

	<div class="contem-colunas">
		<div class="coluna">
			<img src="_imgs/receitas/<?=$detalhes[0]->imagem?>" alt="<?=$detalhes[0]->titulo?>">
		</div>
		<div class="coluna">
			<p>
				<?=$detalhes[0]->ingredientes?>
			</p>
		</div>
	</div>

	<h4>Modo de preparo</h4>
	<p>
		<?=$detalhes[0]->preparo?>
	</p>

	<div class="barra-share">

		<a href="receitas" class="nav-link w133 inline" title="Voltar para Receitas">VOLTAR</a>

		<a href="#" onclick="window.print(); return false;" class="nav-link w133 inline espacado" title="Imprimir esta Receita">IMPRIMIR</a>

		<a href="#" onclick="window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog', 'width=626,height=436'); return false;" class="nav-link w133 inline" title="Compartilhar esta Receita">COMPARTILHAR</a>

	</div>
</div>