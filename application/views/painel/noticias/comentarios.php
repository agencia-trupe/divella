<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>    
  </div>

  <a href="painel/blog/index" class="btn btn-voltar">← voltar</a>

  <br><br>

  <div class="row">
    <div class="span12 columns">

      <?php if ($comentarios): ?>

        <table class="table table-striped table-bordered table-condensed">

          <thead>
            <tr>
            	<th class="yellow header headerSortDown">Nome</th>
            	<th class="header">E-mail</th>
            	<th class="header">Data</th>
            	<th class="header">Comentário</th>
            	<th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($comentarios as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                	<td><?=$value->nome?></td>
                	<td><?=$value->email?></td>
                  	<td><?=dt2horas($value->datetime)?></td>
                  	<td><?=$value->comentario?></td>
                  	<td class="crud-actions" style="width:90px; text-align:center;">
                    	<a href="painel/<?=$this->router->class?>/excluirComentarios/<?=$value->id.'/'.$atual->id?>" class="btn btn-danger btn-delete">excluir</a>
                  	</td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

        <h2>Nenhum Comentário</h2>

      <?php endif ?>

    </div>
  </div>