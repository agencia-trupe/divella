<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/banners/index/">Banners</a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/banners/form/"><?=$titulo?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Link
		<input type="text" name="link" required value="<?=$registro->link?>"></label>

		<label>Imagem (552px x 552px)
			<?php if ($registro->imagem): ?>
				<br><img src="_imgs/home/<?=$registro->imagem?>"><br>
			<?php endif ?>
		<input type="file" name="userfile"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título
		<input type="text" name="titulo" required autofocus></label>

		<label>Link
		<input type="text" name="link" required></label>

		<label>Imagem (552px x 552px)
		<input type="file" name="userfile" required></label>		

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>