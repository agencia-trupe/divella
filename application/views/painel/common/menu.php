<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

      <a href="painel/home" class="brand">Divella</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li <?if($this->router->class=='cadastros')echo" class='active'"?>><a href="painel/cadastros">Cadastros</a></li>

        <li <?if($this->router->class=='receitas')echo" class='active'"?>><a href="painel/receitas">Receitas</a></li>

        <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>