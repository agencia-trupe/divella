<div class="modal" id="modal-login">

  <form action="painel/home/login" method="post">

    <div class="modal-header">
      <h3>
        Painel Administrativo - Divella
      </h3>
    </div>

    <div class="modal-body">

      <fieldset>

        <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
          <div class="alert alert-block alert-success fade in"><?=$mostrarsucesso?></div>
        <?elseif(isset($mostrarerro) && $mostrarerro):?>
          <div class="alert alert-block alert-error fade in"><?=$mostrarerro?></div>
        <?endif;?>

        <div class="control-group">
          <label for="login-username">Usuário</label>
          <div class="input">
            <input style="width:350px" class="large validates[required]" required id="login-username" name="usuario" size="30" type="text" autocomplete="off">
          </div>
        </div>

        <div class="control-group error">
          <label for="login-password">Senha</label>
          <div class="input">
            <input style="width:350px" class="large validates[required]" required id="login-password" name="senha" size="30" type="password">
          </div>
        </div>

      </fieldset>

    </div>

    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Login</button>
    </div>

  </form>

</div>

</body></html>