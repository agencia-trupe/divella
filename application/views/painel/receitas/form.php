<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Imagem<br>
		<?php if($registro->imagem): ?>
			<img src="_imgs/receitas/<?=$registro->imagem?>"><br>
		<?php endif; ?>
		<input type="file" name="userfile"></label>

		<label>Ingredientes<br>
		<textarea name="ingredientes" class="medio basico"><?=$registro->ingredientes?></textarea></label>

		<label>Preparo<br>
		<textarea name="preparo" class="medio basico"><?=$registro->preparo?></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" required></label>

		<label>Imagem<br>
		<input type="text" name="imagem" required></label>

		<label>Ingredientes<br>
		<textarea name="ingredientes" class="medio basico"></textarea></label>

		<label>Preparo<br>
		<textarea name="preparo" class="medio basico"></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>