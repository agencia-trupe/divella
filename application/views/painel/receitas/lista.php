<div class="container top">

    <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
        <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
    <?elseif(isset($mostrarerro) && $mostrarerro):?>
        <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
    <?endif;?>

    <div class="page-header users-header">
        <h2>
            <?=$titulo?>
        </h2>
    </div>

    <div class="row">
        <div class="span12 columns">

            <?php if ($registros): ?>

            <p class="muted">Para ordenar as Receitas clique no botão 'mover' e arraste a Receita para o local desejado.</p>

            <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="receitas">

                <thead>
                    <tr>
                        <th>Ordenar</th>
                        <th>Título</th>
                        <th>Ações</th>
                    </tr>
                </thead>

                <tbody>
                <?php foreach ($registros as $key => $value): ?>

                    <tr class="tr-row" id="row_<?=$value->id?>">
                        <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                        <td><?=$value->titulo?></td>
                        <td style="width:55px">
                            <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                        </td>
                    </tr>

                <?php endforeach ?>
                </tbody>

            </table>

            <?php else:?>

                <h3>Nenhuma Receita</h2>

            <?php endif ?>

        </div>
    </div>