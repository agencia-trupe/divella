<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
	<h2>
	  <?=$titulo?>
	</h2>
  </div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/categoriasAlterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título da Categoria
		<input type="text" name="titulo" value="<?=$registro->titulo?>"></label>

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Salvar</button>
			<button class="btn btn-voltar" type="reset">Voltar</button>
		</div>

	</form>

<?else:?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/categoriasInserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título da Categoria
		<input type="text" name="titulo"></label>

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Salvar</button>
			<button class="btn btn-voltar" type="reset">Voltar</button>
		</div>

	</form>

<?endif;?>