<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>Data<br>
		<input type="text" autocomplete="off" name="data" required class="datepicker" value="<?=formataData($registro->data, 'mysql2br')?>"></label>

		<label>Categoria<br>
			<select name="id_blog_categorias" required>
				<option value="">Selecione a Categoria do Post</option>
				<?php foreach ($categorias as $key => $value): ?>
					<option value="<?=$value->id?>" <?if($value->id==$registro->id_blog_categorias)echo" selected"?>><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>

		<label>Texto<br>
		<textarea name="texto" class="medio completo"><?=$registro->texto?></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>Data<br>
		<input type="text" autocomplete="off" name="data" required class="datepicker"></label>

		<label>Categoria<br>
			<select name="id_blog_categorias" required>
				<option value="">Selecione a Categoria do Post</option>
				<?php foreach ($categorias as $key => $value): ?>
					<option value="<?=$value->id?>"><?=$value->titulo?></option>
				<?php endforeach ?>
			</select>
		</label>

		<label>Texto<br>
		<textarea name="texto" class="medio completo"></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>
