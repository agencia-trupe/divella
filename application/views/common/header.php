<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title><? echo (isset($title) && $title) ? $title : "Divella - Viaggia che ti fa bene!"?></title>
  <meta name="description" content="<? echo (isset($description) && $description) ? $description : "DIVELLA LEVA VOCÊ E UM ACOMPANHANTE PARA A ITÁLIA EM UMA VIAGEM INESQUECÍVEL"?>">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2013 Trupe Design" />

  <meta name="viewport" content="width=1200px,initial-scale=1">

  <meta property="og:title" content="<? echo (isset($title) && $title) ? $title : "Divella - Viaggia che ti fa bene!"?>"/>
  <meta property="og:site_name" content="Divella"/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<? echo (isset($image) && $image) ? base_url($image) : base_url('_imgs/layout/marca-divella.png')?>"/>
  <meta property="og:url" content="<?=current_url()?>"/>
  <meta property="og:description" content="<? echo (isset($description) && $description) ? $description : "DIVELLA LEVA VOCÊ E UM ACOMPANHANTE PARA A ITÁLIA EM UMA VIAGEM INESQUECÍVEL"?>"/>

  <meta property="fb:admins" content="100002297057504"/>
  <meta property="fb:app_id" content="440398699405323"/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <link href='http://fonts.googleapis.com/css?family=Libre+Baskerville:400,700' rel='stylesheet' type='text/css'>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', 'fancybox/fancybox', $this->router->class, $load_css))?>


  <?if(ENVIRONMENT == 'development'):?>

    <?JS(array('modernizr-2.0.6.min', 'less-1.4.1.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>

  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=440398699405323";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <header>
    <div class="centro">
      <div class="fb-like" data-href="https://www.facebook.com/divellabrasil" data-width="110" data-layout="button_count" data-show-faces="false" data-send="false"></div>
    </div>
  </header>