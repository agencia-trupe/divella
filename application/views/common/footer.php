    </div> <!-- fim da div conteudo -->

  <footer>
    <div class="centro">
      <a href="http://www.lapastina.com.br" target="_blank" title="La Pastina" id="link-lapastina"></a>
    </div>
  </footer>


  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('imagesloaded.pkgd.min', 'jquery.maskedinput.min', 'fancybox', 'front'))?>

</body>
</html>
