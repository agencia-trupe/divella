<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->view('cadastro/cadastro');
    }

    function enviar(){

    	$msg_erro = "";

		// resposta
    	$resposta = $this->input->post('resposta');
    	if(!$resposta) $msg_erro = "Responda a pergunta para poder participar!";

		// nome
		$nome = $this->input->post('nome');
		if(!$nome && !$msg_erro) $msg_erro = "Informe seu nome!";

		// cpf
		$cpf = $this->input->post('cpf');
		if(!$cpf && !$msg_erro) $msg_erro = "Informe seu cpf!";

		if(!$this->isValid($cpf) && !$msg_erro) $msg_erro = "Informe um cpf válido!";

		// endereco
		$endereco = $this->input->post('endereco');
		if(!$endereco && !$msg_erro) $msg_erro = "Informe seu endereço!";

		// numero
		$numero = $this->input->post('numero');
		if(!$numero && !$msg_erro) $msg_erro = "Informe o número de seu endereço!";

		// complemento
		$complemento = $this->input->post('complemento');

		// bairro
		$bairro = $this->input->post('bairro');
		if(!$bairro && !$msg_erro) $msg_erro = "Informe seu bairro!";

		// cidade
		$cidade = $this->input->post('cidade');
		if(!$cidade && !$msg_erro) $msg_erro = "Informe sua cidade!";

		// estado
		$estado = $this->input->post('estado');
		if(!$estado && !$msg_erro) $msg_erro = "Informe seu estado!";

		// cep
		$cep = $this->input->post('cep');
		if(!$cep && !$msg_erro) $msg_erro = "Informe seu cep!";

		// telefone
		$telefone = $this->input->post('telefone');
		if(!$telefone && !$msg_erro) $msg_erro = "Informe seu telefone!";

		// email
		$email = $this->input->post('email');
		if(!$email && !$msg_erro) $msg_erro = "Informe seu email!";

		// data_nascimento
		$data_nascimento = $this->input->post('data_nascimento');
		if(!$data_nascimento && !$msg_erro) $msg_erro = "Informe sua data de nascimento!";

		if($this->calculaIdade($data_nascimento) < 18 && !$msg_erro) $msg_erro = "É necessário ser maior de 18 anos para participar!";

		// sexo
		$sexo = $this->input->post('sexo');
		if(!$sexo && !$msg_erro) $msg_erro = "Informe seu sexo!";

		// local_compra
		$local_compra = $this->input->post('local_compra');
		if(!$local_compra && !$msg_erro) $msg_erro = "Informe o local da compra do produto!";

		// produtos
		$produto1 = "";
		for ($i=1; $i <= 13 ; $i++) {
			$digito = $this->input->post('prod_1_input_'.$i);
			if(ctype_digit($digito))
				$produto1 .= $digito;
			else
				if(!$msg_erro) $msg_erro = "Informe todos os números dos 3 códigos de barra para poder participar!";
		}

		// $check_produto1 = $this->db->where('cod_produto_1', $produto1)
		// 						  ->or_where('cod_produto_2', $produto1)
		// 						  ->or_where('cod_produto_3', $produto1)
		// 						  ->get('cadastros')
		// 						  ->num_rows();

		// if($check_produto1 > 0 && !$msg_erro) $msg_erro = "Este código de barra já foi cadastrado! $produto1";

		$produto2 = "";
		for ($i=1; $i <= 13 ; $i++) {
			$digito = $this->input->post('prod_2_input_'.$i);
			if(ctype_digit($digito))
				$produto2 .= $digito;
			else
				if(!$msg_erro) $msg_erro = "Informe todos os números dos 3 códigos de barra para poder participar!";
		}

		// $check_produto2 = $this->db->where('cod_produto_1', $produto2)
		// 						  ->or_where('cod_produto_2', $produto2)
		// 						  ->or_where('cod_produto_3', $produto2)
		// 						  ->get('cadastros')
		// 						  ->num_rows();

		// if($check_produto2 > 0 && !$msg_erro) $msg_erro = "Este código de barra já foi cadastrado! $produto2";

		$produto3 = "";
		for ($i=1; $i <= 13 ; $i++) {
			$digito = $this->input->post('prod_3_input_'.$i);
			if(ctype_digit($digito))
				$produto3 .= $digito;
			else
				if(!$msg_erro) $msg_erro = "Informe todos os números dos 3 códigos de barra para poder participar!";
		}

		// $check_produto3 = $this->db->where('cod_produto_1', $produto3)
		// 						  ->or_where('cod_produto_2', $produto3)
		// 						  ->or_where('cod_produto_3', $produto3)
		// 						  ->get('cadastros')
		// 						  ->num_rows();

		// if($check_produto3 > 0 && !$msg_erro) $msg_erro = "Este código de barra já foi cadastrado! $produto3";

		//if(($produto1 == $produto2 || $produto1 == $produto3 || $produto2 == $produto3) & !$msg_erro) $msg_erro = "Você deve informar 3 códigos de barra diferentes para participar!";

		$aceite_regulamento = $this->input->post('aceite_regulamento');
		if($aceite_regulamento == 0 && !$msg_erro) $msg_erro = "Você deve aceitar os termos do regulamento para participar!";

		$receber_newsletter = $this->input->post('receber_newsletter');

		if($msg_erro){
			$this->session->set_flashdata('resposta', $resposta);
			$this->session->set_flashdata('nome', $nome);
			$this->session->set_flashdata('cpf', $cpf);
			$this->session->set_flashdata('endereco', $endereco);
			$this->session->set_flashdata('numero', $numero);
			$this->session->set_flashdata('complemento', $complemento);
			$this->session->set_flashdata('bairro', $bairro);
			$this->session->set_flashdata('cidade', $cidade);
			$this->session->set_flashdata('estado', $estado);
			$this->session->set_flashdata('cep', $cep);
			$this->session->set_flashdata('telefone', $telefone);
			$this->session->set_flashdata('email', $email);
			$this->session->set_flashdata('data_nascimento', $data_nascimento);
			$this->session->set_flashdata('sexo', $sexo);
			$this->session->set_flashdata('local_compra', $local_compra);
			for ($i=1; $i <= 3; $i++) {
				for ($k=1; $k <= 13; $k++) {
					$this->session->set_flashdata('prod_'.$i.'_input_'.$k, $this->input->post('prod_'.$i.'_input_'.$k));
				}
			}
			$this->session->set_flashdata('aceite_regulamento', $aceite_regulamento);
			$this->session->set_flashdata('receber_newsletter', $receber_newsletter);
			$this->session->set_flashdata('msg_erro', $msg_erro);
			redirect('cadastro/index', 'refresh');
		}else{
			$insert = $this->db->set('resposta', $resposta)
								->set('nome', $nome)
								->set('cpf', $cpf)
								->set('endereco', $endereco)
								->set('numero', $numero)
								->set('complemento', $complemento)
								->set('bairro', $bairro)
								->set('cidade', $cidade)
								->set('estado', $estado)
								->set('cep', $cep)
								->set('telefone', $telefone)
								->set('email', $email)
								->set('data_nascimento', formataData($data_nascimento, 'br2mysql'))
								->set('sexo', $sexo)
								->set('local_compra', $local_compra)
								->set('cod_produto_1', $produto1)
								->set('cod_produto_2', $produto2)
								->set('cod_produto_3', $produto3)
								->set('aceite_regulamento', $aceite_regulamento)
								->set('receber_newsletter', $receber_newsletter)
								->set('data_cadastro', Date('Y-m-d H:i:s'))
								->set('ip_cadastro', ip())
								->insert('cadastros');

			if($insert){
				$this->session->set_flashdata('cadastro_finalizado', 'S');
				$this->enviarConfirmacao(array(
					'resposta' => $resposta,
					'nome' => $nome,
					'cpf' => $cpf,
					'endereco' => $endereco,
					'numero' => $numero,
					'complemento' => $complemento,
					'bairro' => $bairro,
					'cidade' => $cidade,
					'estado' => $estado,
					'cep' => $cep,
					'telefone' => $telefone,
					'email' => $email,
					'data_nascimento' => $data_nascimento,
					'sexo' => $sexo,
					'local_compra' => $local_compra,
					'cod_produto_1' => $produto1,
					'cod_produto_2' => $produto2,
					'cod_produto_3' => $produto3,
					'receber_newsletter' => ($receber_newsletter == 1) ? 'Sim' : 'Não',
					'data_cadastro' => Date('d/m/Y')
				));
			}else
				$this->session->set_flashdata('cadastro_finalizado', 'N');

			redirect('cadastro/finalizado');
		}
    }

    function finalizado(){
    	$this->load->view('cadastro/finalizado');
    }

    private function isValid($cpf){
		$cpf = str_pad(preg_replace('/[^0-9]/i', '', $cpf), 11, '0', STR_PAD_LEFT);

		if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999'){
			return false;
    	}
		else{
	        for ($t = 9; $t < 11; $t++) {
	            for ($d = 0, $c = 0; $c < $t; $c++) {
	                $d += $cpf{$c} * (($t + 1) - $c);
	            }

	            $d = ((10 * $d) % 11) % 10;

	            if ($cpf{$c} != $d) {
	                return false;
	            }
	        }

        	return true;
    	}
    }

    private function calculaIdade($data_nascimento){
    	if(!$data_nascimento) return '0';

    	$birthDate = $data_nascimento;
        $birthDate = explode("/", $birthDate);
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y")-$birthDate[2])-1):(date("Y")-$birthDate[2]));
        return $age;
    }

    private function enviarConfirmacao($dados){
        $emailconf['charset'] = 'utf-8';
        $emailconf['mailtype'] = 'html';
        $emailconf['protocol'] = 'mail';

		//$emailconf['protocol'] = 'smtp';
        //$emailconf['smtp_host'] = '';
        //$emailconf['smtp_user'] = '';
        //$emailconf['smtp_pass'] = '';

        $this->load->library('email');

        $this->email->initialize($emailconf);

        $from = 'campanhas@lapastina.com';
        $fromname = 'Divella - Viaggia che ti fa bene!';
        $to = $dados['email'];
        $bcc = 'bruno@trupe.net';
        $assunto = 'Confirmação de Inscrição na promoção Divella - Viaggia che ti fa bene!';

        $emailhtml = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Sua inscrição na promoção "Divella - Viaggia che ti fa bene!" foi recebida com sucesso.</title>
    <meta charset="utf-8">
</head>
<body>
	<h1>Sua inscrição na promoção "Divella - Viaggia che ti fa bene!" foi recebida com sucesso.</h1>
	<h2>Segue a confirmação dos dados cadastrados para você acompanhar e torcer!</h2>
	<p>
		<strong>QUAL É A MASSA GRANO DURO 100% ITALIANA NO BRASIL?</strong> : {$dados['resposta']}<br>
		Nome :  {$dados['nome']} <br>
		CPF :  {$dados['cpf']} <br>
		Endereço :  {$dados['endereco']} <br>
		Número :  {$dados['numero']} <br>
		Complemento :  {$dados['complemento']} <br>
		Bairro :  {$dados['bairro']} <br>
		Cidade :  {$dados['cidade']} <br>
		Estado :  {$dados['estado']} <br>
		CEP :  {$dados['cep']} <br>
		Telefone :  {$dados['telefone']} <br>
		E-mail :  {$dados['email']} <br>
		Data de Nascimento :  {$dados['data_nascimento']} <br>
		Sexo :  {$dados['sexo']} <br>
		Local de Compra :  {$dados['local_compra']} <br>
		Código de barra do produto 1 :  {$dados['cod_produto_1']} <br>
		Código de barra do produto 2 :  {$dados['cod_produto_2']} <br>
		Código de barra do produto 3 :  {$dados['cod_produto_3']} <br>
		Receber informações e promoções de Divella :  {$dados['receber_newsletter']} <br>
		Data de inscrição :  {$dados['data_cadastro']} <br>
	</p>
	<h3>BOA SORTE!</h3>
	<h4>Divella</h4>
</body>
</html>
EML;

        $plain = <<<EML
Sua inscrição na promoção "Divella - Viaggia che ti fa bene!" foi recebida com sucesso.\r\n
Segue a confirmação dos dados cadastrados para você acompanhar e torcer!\r\n
\r\n
QUAL É A MASSA GRANO DURO 100% ITALIANA NO BRASIL? : {$dados['resposta']}\r\n
Nome :  {$dados['nome']} \r\n
CPF :  {$dados['cpf']} \r\n
Endereço :  {$dados['endereco']} \r\n
Número :  {$dados['numero']} \r\n
Complemento :  {$dados['complemento']} \r\n
Bairro :  {$dados['bairro']} \r\n
Cidade :  {$dados['cidade']} \r\n
Estado :  {$dados['estado']} \r\n
CEP :  {$dados['cep']} \r\n
Telefone :  {$dados['telefone']} \r\n
E-mail :  {$dados['email']} \r\n
Data de Nascimento :  {$dados['data_nascimento']} \r\n
Sexo :  {$dados['sexo']} \r\n
Local de Compra :  {$dados['local_compra']} \r\n
Código de barra do produto 1 :  {$dados['cod_produto_1']} \r\n
Código de barra do produto 2 :  {$dados['cod_produto_2']} \r\n
Código de barra do produto 3 :  {$dados['cod_produto_3']} \r\n
Receber informações e promoções de Divella :  {$dados['receber_newsletter']} \r\n
Data de inscrição :  {$dados['data_cadastro']} \r\n
\r\n
BOA SORTE!\r\n
Divella\r\n
EML;

        $this->email->from($from, $fromname);
        $this->email->to($to);

        if($bcc)
            $this->email->bcc($bcc);

        $this->email->reply_to($from);

        $this->email->subject($assunto);
        $this->email->message($emailhtml);
        $this->email->set_alt_message($plain);

        $this->email->send();
    }
}