<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('cadastros_model', 'cadastros');
   }

   function index($pag = 0){
   	$this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("painel/cadastros/index/"),
         'per_page' => 20,
         'uri_segment' => 4,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->cadastros->numeroResultados()
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      $data['registros'] = $this->cadastros->pegarPaginado($pag_options['per_page'], $pag);



      $data['titulo'] = "Cadastros";
      $data['unidade'] = "Relatório de cadastros";
      $data['campo_1'] = "Usuário";
      $data['campo_2'] = "E-mail";

      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;

      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
      else
         $data['mostrarsucesso'] = false;

      $this->load->view('painel/cadastros/lista', $data);
   }

   function extrair(){
      $this->hasLayout = FALSE;
      $this->load->dbutil();

$str_query = <<<STR
SELECT
resposta as Resposta,
nome as Nome,
cpf as CPF,
endereco as Endereço,
numero as Número,
complemento as Complemento,
bairro as Bairro,
cidade as Cidade,
estado as Estado,
cep as CEP,
telefone as Telefone,
email as 'E-mail',
DATE_FORMAT( data_nascimento, '%d/%m/%Y' )  as 'Data de Nascimento',
sexo as Sexo,
local_compra as 'Local de Compra',
CONCAT('cod: ', cod_produto_1) as 'Código de Barra 1',
CONCAT('cod: ', cod_produto_2) as 'Código de Barra 2',
CONCAT('cod: ', cod_produto_3) as 'Código de Barra 3',
(CASE aceite_regulamento
WHEN 0 THEN 'Não'
WHEN 1 THEN 'Sim'
END) as 'Aceite do Regulamento',
(CASE receber_newsletter
WHEN 0 THEN 'Não'
WHEN 1 THEN 'Sim'
END) as 'Receber Newsletter',
DATE_FORMAT( data_cadastro, '%d/%m/%Y %H:%i:%s' )  as 'Data de Cadastro',
ip_cadastro as 'IP de Cadastro'
FROM cadastros
WHERE excluido = 0
ORDER BY nome ASC
STR;

      $users = $this->db->query($str_query);
      $filename = "cadastros_".Date('d-m-Y_H-i-s').'.csv';

      $delimiter = ";";
      $newline = "\r\n";

      $this->output->set_header('Content-Type: application/force-download');
      $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
      $this->output->set_content_type('text/csv')->set_output(utf8_decode($this->dbutil->csv_from_result($users, $delimiter, $newline)), "ISO-8859-1", "UTF-8");
   }

   function excluir($id){
	   if($this->cadastros->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      redirect('painel/cadastros/index/', 'refresh');
   }

}