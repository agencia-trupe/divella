<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receitas extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Receitas";
		$this->unidade = "Receita";
		$this->load->model('receitas_model', 'model');
	}

}