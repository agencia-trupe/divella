   <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('noticias_model', 'model');
   }

    function comentarios($id_noticia, $pag = 0){
        $this->load->library('pagination');

          $pag_options = array(
             'base_url' => base_url("painel/".$this->router->class."/comentarios/".$id_noticia.'/'),
             'per_page' => 20,
             'uri_segment' => 5,
             'next_link' => "Próxima →",
             'next_tag_open' => "<li class='next'>",
             'next_tag_close' => '</li>',
             'prev_link' => "← Anterior",
             'prev_tag_open' => "<li class='prev'>",
             'prev_tag_close' => '</li>',
             'display_pages' => TRUE,
             'num_links' => 10,
             'first_link' => FALSE,
             'last_link' => FALSE,
             'num_tag_open' => '<li>',
             'num_tag_close' => '</li>',
             'cur_tag_open' => '<li><b>',
             'cur_tag_close' => '</b></li>',
             'total_rows' => $this->model->pegarComentarios($id_noticia, TRUE)
          );

          $this->pagination->initialize($pag_options);
          $data['paginacao'] = $this->pagination->create_links();

          $data['registros'] = $this->model->pegarComentariosPaginados($id_noticia, $pag_options['per_page'], $pag);

          if($this->session->flashdata('mostrarerro') === true)
             $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
          else
             $data['mostrarerro'] = false;
         
          if($this->session->flashdata('mostrarsucesso') === true)
             $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
          else
             $data['mostrarsucesso'] = false;

        $this->load->view('painel/'.$this->router->class.'/comentarios', $data);
    }

	function excluirComentarios($id, $id_noti){	
		if($this->model->excluirComentario($id)){
        	$this->session->set_flashdata('mostrarsucesso', true);
        	$this->session->set_flashdata('mostrarsucesso_mensagem', 'Comentário excluido com sucesso');
      	}else{
        	$this->session->set_flashdata('mostrarerro', true);
        	$this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir comentário');
      	}
   		
   		redirect('painel/'.$this->router->class.'/comentarios/'.$id_noti);
	}
}