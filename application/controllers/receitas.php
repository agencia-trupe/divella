<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Receitas extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index($slug = false){

    	if ($slug) {

    		$data['detalhes'] = $this->db->get_where('receitas', array('slug' => $slug))->result();

            if(!$data['detalhes'] || !isset($data['detalhes'][0]))
    			redirect('receitas');
    		else{

                $this->headervar['title'] = "Divella - ".$data['detalhes'][0]->titulo;
                $this->headervar['description'] = "Confira a Receita de ".$data['detalhes'][0]->titulo;
                $this->headervar['image'] = "_imgs/receitas/".$data['detalhes'][0]->imagem;

    			$this->load->view('receitas/receitas-detalhes', $data);
            }

    	}else{
    		$data['receitas'] = $this->db->order_by('ordem', 'asc')->get('receitas')->result();
    		$this->load->view('receitas/receitas', $data);
    	}
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
            redirect('painel/receitas/index');
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function inserir(){
        redirect('painel/receitas/index');
        // if($this->model->inserir()){
        //     $this->session->set_flashdata('mostrarsucesso', true);
        //     $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        // }else{
        //     $this->session->set_flashdata('mostrarerro', true);
        //     $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        // }

        // redirect('painel/'.$this->router->class.'/index', 'refresh');
    }

}