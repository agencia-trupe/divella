var ajustaAltura = function(bodyHeight, browserHeight){
	if(browserHeight > bodyHeight){
		var diff = (browserHeight - bodyHeight);
		if($('.lista-receitas a').length){
			imagesLoaded(  document.querySelectorAll('.lista-receitas a'), function() {
				$('#conteudo').css('min-height', parseInt($('#conteudo').css('height')) + diff);
			});
		}else{
			$('#conteudo').css('min-height', parseInt($('#conteudo').css('height')) + diff);
		}
	}
}

function alert(message) {
    $('#alert').html(message).addClass('aberto');
    setTimeout( function(){
    	$('#alert').removeClass('aberto');
    }, 6000);
}

function calculateAge(dobString) {
    var dob = new Date(dobString);
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
    var age = currentYear - dob.getFullYear();

    if(birthdayThisYear > currentDate) {
        age--;
    }

    return age;
}

function validarCPF(cpf) {

    cpf = cpf.replace(/[^\d]+/g,'');

    if(cpf == '') return false;

    // Elimina CPFs invalidos conhecidos
    if (cpf.length != 11 ||
        cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999")
        return false;

    // Valida 1o digito
    add = 0;
    for (i=0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;

    // Valida 2o digito
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;

    return true;
}

$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	var browserHeight = $(window).height();
	ajustaAltura(parseInt($('body').css('height')),browserHeight);

	$(window).resize( function(){
		ajustaAltura(parseInt($('body').css('height')),browserHeight);
	});

	if($('#alert.aberto').length){
		setTimeout( function(){
    		$('#alert').removeClass('aberto');
    	}, 6000);
	}

	$('.shadow').fancybox({
		'width' : 580,
		'autoDimensions' : false,
		'title' : "Regulamento"
	});

	$('#input-cpf').mask("999.999.999-99");
	$('#input-cep').mask("99999-999");
	$('#input-telefone').mask("(99) 9999-9999?9");
	$('#input-data_nascimento').mask("99/99/9999");
	$('#prod_1_input_1').mask("9");
	$('#prod_1_input_2').mask("9");
	$('#prod_1_input_3').mask("9");
	$('#prod_1_input_4').mask("9");
	$('#prod_1_input_5').mask("9");
	$('#prod_1_input_6').mask("9");
	$('#prod_1_input_7').mask("9");
	$('#prod_1_input_8').mask("9");
	$('#prod_1_input_9').mask("9");
	$('#prod_1_input_10').mask("9");
	$('#prod_1_input_11').mask("9");
	$('#prod_1_input_12').mask("9");
	$('#prod_1_input_13').mask("9");
	$('#prod_2_input_1').mask("9");
	$('#prod_2_input_2').mask("9");
	$('#prod_2_input_3').mask("9");
	$('#prod_2_input_4').mask("9");
	$('#prod_2_input_5').mask("9");
	$('#prod_2_input_6').mask("9");
	$('#prod_2_input_7').mask("9");
	$('#prod_2_input_8').mask("9");
	$('#prod_2_input_9').mask("9");
	$('#prod_2_input_10').mask("9");
	$('#prod_2_input_11').mask("9");
	$('#prod_2_input_12').mask("9");
	$('#prod_2_input_13').mask("9");
	$('#prod_3_input_1').mask("9");
	$('#prod_3_input_2').mask("9");
	$('#prod_3_input_3').mask("9");
	$('#prod_3_input_4').mask("9");
	$('#prod_3_input_5').mask("9");
	$('#prod_3_input_6').mask("9");
	$('#prod_3_input_7').mask("9");
	$('#prod_3_input_8').mask("9");
	$('#prod_3_input_9').mask("9");
	$('#prod_3_input_10').mask("9");
	$('#prod_3_input_11').mask("9");
	$('#prod_3_input_12').mask("9");
	$('#prod_3_input_13').mask("9");

	$('.codigos input').keyup( function(){
		if($(this).val() != '_'){
			if($(this).next().length)
				$(this).next().focus();
			else
				$(this).parent().next().find('input:first').focus();
		}
	});

	$('.logradouro').removeClass('aberto');

	$('#consultar-cep').click( function(e){
		e.preventDefault();
		var cep = $('#input-cep').val();
		var re = /(\d){5}-(\d){3}/;
		var botao = $(this);
		if(re.exec(cep)){
			botao.html("consultando...");
			var cep = cep.replace("-", "");
			$.getJSON("http://cep.correiocontrol.com.br/"+cep+".json", function(data){
				$('.logradouro').addClass('aberto');
				$('#input-bairro').val(data.bairro);
				$('#input-endereco').val(data.logradouro);
				$('#input-estado').val(data.uf);
				$('#input-cidade').val(data.localidade);
			});
			setTimeout( function(){
				botao.html("consultar");
			}, 1000);
		}
	});

	$('#form-cadastro').submit( function(){

		$('input[type="submit"]').attr('disabled', 'disabled')

		if($('input[name="resposta"]:checked').length === 0){
			alert('Responda a pergunta para poder participar!');
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($('#input-nome').val() == ''){
			alert("Informe seu nome!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($('#input-cpf').val() == '' || $('#input-cpf').val() == '___.___.___-__'){
			alert("Informe seu cpf");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if(!validarCPF($('#input-cpf').val())){
			alert("Informe um cpf válido!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($('#input-endereco').val() == ''){
			alert("Informe seu endereço!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($('#input-numero').val() == ''){
			alert("Informe o número do endereço!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-bairro").val() == ''){
			alert("Informe o bairro!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-cidade").val() == ''){
			alert("Informe a cidade!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-estado").val() == ''){
			alert("Informe o estado!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-cep").val() == '' || $("#input-cep").val() == '_____-___'){
			alert("Informe o CEP!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-telefone").val() == '' || $("#input-telefone").val() == '(__) ____-_____'){
			alert("Informe um telefone de contato!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-email").val() == ''){
			alert("Informe um email de contato!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-data_nascimento").val() == '' || $("#input-data_nascimento").val() == '__/__/____'){
			alert("Informe sua data de nascimento!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if(calculateAge($("#input-data_nascimento").val()) < 18){
			alert("É necessário ser maior de 18 anos para participar!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($('input[name="sexo"]:checked').length === 0){
			alert('Responda seu sexo!');
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

		if($("#input-local_compra").val() == ''){
			alert("Informe o local de compra dos produtos!");
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}


		for (var i = 1; i <= 3; i++) {
			for (var k = 1; k <= 13; k++) {
				if($('#prod_'+i+'_input_'+k).val() == ''){
					alert('Informe todos os números dos 3 códigos de barra para poder participar!');
					$('input[type="submit"]').removeAttr('disabled')
					return false;
				}else{
					cod[i] += $('#prod_'+i+'_input_'+k).val();
				};
			};
		};

		if($('#aceite_regulamento:checked').length == 0){
			alert('Você deve aceitar os termos do regulamento para participar!');
			$('input[type="submit"]').removeAttr('disabled')
			return false;
		}

	});

	setTimeout( function(){
		$('#aviao').addClass('final');
		setTimeout( function(){
			$('#aviao').addClass('final-apagado');
		}, 4000);
	}, 200);

});