CREATE DATABASE  IF NOT EXISTS `divella2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `divella2`;
-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: divella
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.13.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cadastros`
--

DROP TABLE IF EXISTS `cadastros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadastros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `resposta` varchar(45) DEFAULT NULL,
  `nome` varchar(140) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `endereco` varchar(140) DEFAULT NULL,
  `numero` varchar(10) DEFAULT NULL,
  `complemento` varchar(140) DEFAULT NULL,
  `bairro` varchar(140) DEFAULT NULL,
  `cidade` varchar(140) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `telefone` varchar(17) DEFAULT NULL,
  `email` varchar(140) DEFAULT NULL,
  `data_nascimento` date DEFAULT NULL,
  `sexo` varchar(1) DEFAULT NULL,
  `local_compra` varchar(140) DEFAULT NULL,
  `cod_produto_1` varchar(13) DEFAULT NULL,
  `cod_produto_2` varchar(13) DEFAULT NULL,
  `cod_produto_3` varchar(13) DEFAULT NULL,
  `aceite_regulamento` int(11) NOT NULL DEFAULT '0',
  `receber_newsletter` int(11) NOT NULL DEFAULT '0',
  `data_cadastro` datetime DEFAULT NULL,
  `ip_cadastro` varchar(16) DEFAULT NULL,
  `excluido` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cod_produto_1_UNIQUE` (`cod_produto_1`),
  UNIQUE KEY `cod_produto_2_UNIQUE` (`cod_produto_2`),
  UNIQUE KEY `cod_produto_3_UNIQUE` (`cod_produto_3`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cadastros`
--

LOCK TABLES `cadastros` WRITE;
/*!40000 ALTER TABLE `cadastros` DISABLE KEYS */;
/*!40000 ALTER TABLE `cadastros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(140) DEFAULT NULL,
  `password` varchar(140) DEFAULT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'trupe','3a8498d02c173855686c07087ba49cb4fcf86911','contato@trupe.net');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receitas`
--

DROP TABLE IF EXISTS `receitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `slug` varchar(140) DEFAULT NULL,
  `imagem` varchar(140) DEFAULT NULL,
  `ingredientes` text,
  `preparo` text,
  `ordem` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receitas`
--

LOCK TABLES `receitas` WRITE;
/*!40000 ALTER TABLE `receitas` DISABLE KEYS */;
INSERT INTO `receitas` VALUES (2,'Bolinhos de aipim com rosmarino','Bolinhos-de-aipim-com-rosmarino','bolinho-aipim-com-rosmarino.jpg','<ul>\n<li>500g de mandioca (aipim) descascada e cozida</li>\n<li>180g de queijo de coalho ralado fino</li>\n<li>1 colher (caf&eacute;) de bicarbonato de s&oacute;dio</li>\n<li>2 colheres (sopa) de cebolinha picada&nbsp;</li>\n<li>Sal a gosto</li>\n</ul>\n<p><strong>Para empanar:</strong></p>\n<ul>\n<li>200g de Rosmarino Divella, cozida</li>\n<li>2 colheres (sopa) de farinha de rosca&nbsp;</li>\n<li>1 ovo batido&nbsp;</li>\n</ul>\n<p><strong>Para fritar:</strong></p>\n<ul>\n<li>&Oacute;leo (suficiente para cobrir os bolinhos)</li>\n</ul>\n<p><strong>Para acompanhar:</strong></p>\n<ul>\n<li>Geleia de pimenta</li>\n</ul>','<p>Passe a mandioca cozida por uma peneira fina. Junte ao pur&ecirc; de mandioca o queijo coalho e o restante dos ingredientes dos bolinhos e reserve. Cozinhe o rosmarino em bastante &aacute;gua com sal at&eacute; ficar al dente. Misture o macarr&atilde;o cozido com as duas colheres de farinha de rosca e reserve.</p>\n<p>Separe a massa do bolinho em por&ccedil;&otilde;es de 40g cada um (cerca de 2 colheres de sopa). Molhe os bolinhos um a um, na m&atilde;o, com um pequeno quibe. Passe os bolinhos no ovo batido e em seguida na mistura de mandioca e farinha de rosca.</p>\n<p>Frite-os imediatamente em &oacute;leo quente (170&ordm;C), at&eacute; come&ccedil;ar a dourar. Escorra-os em papel absorvente e sirva ainda quente acompanhados de geleia de pimenta.</p>\n<p>Rendimento: 15 bolinhos<br /> Tempo de preparo: 30 minutos</p>',0),(3,'Ceviche de linguado com farfalle','Ceviche-de-linguado-com-farfalle','ceviche-linguado-farfalle.jpg','<ul>\n<li>60g de massa tipo farfalle Divella</li>\n<li>500g de filet de linguado</li>\n</ul>\n<p><strong>Para o vinagrete:</strong></p>\n<ul>\n<li>2 colheres (ch&aacute;) de salsinha picada</li>\n<li>1 colher (ch&aacute;) de coentro picado</li>\n<li>1 colher (ch&aacute;) de folhas de sals&atilde;o picadas</li>\n<li>Raspas de 2 lim&otilde;es sicilianos</li>\n<li>50ml de suco de lim&atilde;o siciliano ( &frac12; lim&atilde;o)</li>\n<li>40ml de de suco de lima-da-p&eacute;rsia</li>\n<li>2 colheres (sopa) de azeite de oliva extravirgem</li>\n<li>1 colher (ch&aacute;) de pimenta dedo-de-mo&ccedil;a picada</li>\n<li>2 colheres (caf&eacute;) de a&ccedil;&uacute;car</li>\n<li>2 colheres (sopa) de pimenta rosa</li>\n<li>2 colheres (sopa) de carambola picada</li>\n<li>Sal a gosto</li>\n<li>1 lima-da-p&eacute;rsia cortada em gomos</li>\n<li>1 carambola cortada em fatias finas</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar. Cozinhe o farfalle, escorra-o e deixe-o esfriar.</p>\n<p>Enquanto isso corte o linguado em l&acirc;minas bem finas como o sashimi e arrume-as num prato grande. Por cima, espalhe o farfalle.</p>\n<p>Misture os ingredientes do vinagrete e despeje sobre o peixe e a massa. Cubra com filme plastico e deixe marinar, na geladeira por 6 horas.</p>\n<p>Antes de servir, decore com gomos de lima-da-p&eacute;rsia e as estrelas de carambola.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br /> Tempo de preparo: 6h30</p>',1),(4,'Cheese Cake de Pêssego','Cheese-Cake-de-Pessego','cheesecake-pessego.jpg','<ul>\n<li>01 Pacote de Biscoito Integral OTTIMINI DIVELLA</li>\n<li>01 Tablete de Manteiga Derretida</li>\n<li>03 Cream Cheese</li>\n<li>01 Lata de Leite Condensado</li>\n<li>03 Ovos (Gemas Separadas das Claras)</li>\n<li>02 Colheres Sopa de Maizena</li>\n<li>01 Lata de Leite</li>\n<li>02 Lata de P&ecirc;ssego em Calda LA PASTINA</li>\n</ul>','<p>Triturar os biscoitos e em seguida ir misturando com a manteiga at&eacute; o ponto de quando apertar a massa ela ficar firme sem esfarelar.</p>\n<p>Untar uma forma de fundo falso, e ir apertando a massa em todo o fundo e lateral da forma.</p>\n<p>Bater o cream cheese, o leite condensado e as gemas na batedeira at&eacute; formar um creme liso.</p>\n<p>Misturar a maizena no leite at&eacute; dissolver e ir misturando no creme de gemas.</p>\n<p>Bater as claras em neve e misturar delicadamente.</p>\n<p>Colocar esse creme sobre a massa e levar ao forno pr&eacute; aquecido a 200 graus at&eacute; a massa ficar firme e dourar levemente.</p>\n<p>Deixar esfriar.</p>\n<p>Desenformar a cheese cake e decorar com os p&ecirc;ssegos inteiros. Regar com a calda e levar para gelar.</p>\n<p>Rendimento: 8 por&ccedil;&otilde;es</p>',2),(5,'Conchiglioni com carne seca e abóbora','Conchiglioni-com-carne-seca-e-abobora','conchiglioni-recheado.jpg','<ul>\n<li>1 caixa de Conchiglioni Divella</li>\n</ul>\n<p><strong>Para o refogado de carne-seca:</strong></p>\n<ul>\n<li>250g de carne seca dessalgada em &aacute;gua por 24 horas, na geladeira</li>\n<li>1 colher (sopa) de manteiga</li>\n<li>1 colher (sopa) de azeite de oliva extravirgem</li>\n<li>1 colher (sopa) de cebola roxa picada</li>\n<li>&frac12; colher (caf&eacute;) de pimenta dedo-de-mo&ccedil;a picada&nbsp;</li>\n<li>1 colher (sopa) de tomate sem pele e sem sementes picado</li>\n<li>1 colher (sopa) de salsinha picada</li>\n<li>1 colher ( sopa) de cebolinha picada</li>\n</ul>\n<p><strong>Para o pur&ecirc; de ab&oacute;bora:</strong></p>\n<ul>\n<li>500g de ab&oacute;bora moranga sem casca, cortada em peda&ccedil;os, cozida no vapor&nbsp;</li>\n<li>8 colheres (sopa) de azeite de oliva extravirgem La Pastina&nbsp;</li>\n<li>2 colheres (sopa) de cebola roxa picada</li>\n<li>1 dente de alho picado</li>\n<li>2 colheres (sopa) de gengibre em p&oacute;</li>\n<li>2 colheres (ch&aacute;) de coentro em p&oacute;</li>\n<li>noz-moscada ralada a gosto</li>\n<li>2 gotas de molho Tabasco&nbsp;</li>\n<li>Sal a gosto</li>\n</ul>\n<p><strong>Para decorar:</strong></p>\n<ul>\n<li>Folhas de salsinha</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar.</p>\n<p>Cozinhe os conchiglioni e deixe-os esfriar virados para baixo.</p>\n<p>Bata a ab&oacute;bora no processador e reserve. Numa frigideira, coloque o azeite, a cebola, o alho e refogue rapidamente. Acrescente o gengibre e o coentro em p&oacute;. Junte a noz-moscada, o sal e o Tabasco. Deixe o pur&ecirc; reduzir em fogo baixo e reserve.</p>\n<p>Desfie a carne-seca. Numa frigideira, derreta a manteiga junto com o azeite e refogue rapidamente a cebola e a pimenta.</p>\n<p>Em seguida, acrescente a carne-seca e refogue por uns 3 minutos. Junte o tomate, mexa bem e adicione a salsinha e a cebolinha.</p>\n<p>Recheie cada conchiglioni com o pur&ecirc; de ab&oacute;bora e, por cima, ponha um pouco do refogado de carne seca. Decore com folhas de salsinha e sirva quente.</p>\n<p>Rendimento : 6 por&ccedil;&otilde;es</p>',3),(6,'Couscous Marroquino com frango defumado desfiado','Couscous-Marroquino-com-frango-defumado-desfiado','couscous-marroquino-frango-defumado.jpg','<ul>\n<li>1 Caixa de Couscous DIVELLA</li>\n<li>1 Frango Defumado Desfiado</li>\n<li>1 Vidro de Piment&atilde;o Ao Forno LA PASTINA</li>\n<li>&frac12; Vidro de Alho em Conserva LA PASTINA Cortado em Fatias</li>\n<li>1 Vidro de Azeitonas Verdes Fatiadas LA PASTINA</li>\n<li>2 Tomates sem Semente Cortados em Cubos</li>\n<li>1 Cenoura Ralada</li>\n<li>2 Colheres de Sopa de Tomilho Fresco</li>\n<li>Sal&nbsp;</li>\n<li>Azeite</li>\n</ul>\n<p><strong>Para o caldo de Legumes</strong></p>\n<ul>\n<li>1 Litro de &Aacute;gua</li>\n<li>1 Cenoura Cortada Grosseiramente</li>\n<li>1 Cebola Cortada em 4 Partes</li>\n<li>2 Talos de Sals&atilde;o</li>\n<li>1 Alho Por&oacute;</li>\n</ul>','<p>Colocar o couscous em uma travessa rasa.</p>\n<p>Aquecer 500ml de caldo de legumes e jogar sobre os gr&atilde;os, deixando hidratar por alguns minutos, at&eacute; o caldo ser absorbido.</p>\n<p>Regar com azeite e ir soltando o couscous com o garfo.</p>\n<p>Misturar o restante dos ingredientes , temperando com sal e azeite.</p>\n<p><strong>Para o caldo de legumes:</strong></p>\n<p>Levar todos os ingredientes ao fogo baixo e deixar ferver por 20 minutos.</p>\n<p>Coar e reservar para uso. O que sobrar pode ser congelado</p>',4),(7,'Fusilli com molho vermelho, atum, alcaparras e azeitonas verdes','Fusilli-com-molho-vermelho-atum-alcaparras-e-azeitonas-verdes','fusilli-molho-vermelho-atum-alcaparras-azeitonas-verdes.jpg','<ul>\n<li>1 Pacote de Fusilli DIVELLA</li>\n<li>2 Latas de Pomodori Cubetti LA PASTINA</li>\n<li>2 Latas de Atum Claro ALBO</li>\n<li>&frac12; Vidro de Alcaparras LA PASTINA</li>\n<li>&frac12; Vidro de Azeitonas Verdes Fatiadas LA PASTINA</li>\n<li>1 Cebola Picada</li>\n<li>&frac12; Ma&ccedil;o de Manjeric&atilde;o</li>\n<li>Sal</li>\n<li>Azeite</li>\n<li>Pimenta Negra CARMENCITA</li>\n</ul>','<p>Levar a massa para cozinhar em uma panela com &aacute;gua fervente.<br />Em outra panela, colocar o azeite e refogar a cebola.<br />Adicionar o molho de tomate cubetti e deixar apurar por 5 min em fogo baixo.<br />Finalizar colocando as alcaparras, azeitonas e por ultimo as lascas de atum.<br />Temperar com a pimenta mo&iacute;da na hora e corrigir o sal.<br />Salpicar as folhas de manjeric&atilde;o e servir.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br />Tempo de Preparo: 20 minutos</p>',5),(8,'Lasanha de funghi com creme de quatro queijos trufado ','Lasanha-de-funghi-com-creme-de-quatro-queijos-trufado-','lasanha-funghi-creme-quatro-queijos-trufado.jpg','<ul>\n<li>1 Pacote de Lasanha DIVELLA</li>\n<li>500gr De Funghi Porcini Secchi LA PASTINA</li>\n<li>1 Vidro de Creme 4 Queijos com Trufas LA PASTINA</li>\n<li>&frac12; Vidro de Creme de Leite Fresco</li>\n<li>200gr de Parmes&atilde;o</li>\n<li>&frac12; Vidro de Passata DIVELLA</li>\n<li>Sal</li>\n<li>Azeite</li>\n</ul>','<p>Colocar os funghis em um bowl. Aquecer 2 x&iacute;caras de &aacute;gua e jogar sobre os funghis, deixando hidratar( amolecer)<br />Tirar da &aacute;gua e fatiar. Reserve a agua.<br />Em uma panela, colocar o queijo e o creme de leite, deixando ferver. Adicione os funghis, um pouco da agua reservada , para dar cor , mexer e temperar com sal e pimenta.<br />Em um pirex, untar com azeite, e dispor as placas de lasanha. Cobrir com o creme de funghi e um pouco de parmes&atilde;o. Colocar outra placa, e ir continuando as camadas , at&eacute; finalizar com o creme de funghi e o parmes&atilde;o. Levar ao forno at&eacute; gratinar.<br />Op&ccedil;&atilde;o:<br />Usar um pouco da Passata misturando com o creme de queijo e cobrir a lasanha com o molho ros&eacute; e parmes&atilde;o.</p>\n<p>Rendimento: 5 por&ccedil;&otilde;es<br />Tempo de Preparo: 50 minutos</p>',6),(9,'Paternostri com lulas, feijão branco e marisco','Paternostri-com-lulas-feijao-branco-e-marisco','paternostini-lulas-feijao-branco-marisco.jpg','<ul>\n<li>320g de Paternostri Divella</li>\n<li>500g de marisco</li>\n<li>500g de lulas em peda&ccedil;&otilde;s Albo</li>\n<li>350g de tomates-cereja</li>\n<li>300g de feij&atilde;o branco cozido</li>\n<li>6 colheres (sopa) de azeite de oliva extravirgem La Pastina</li>\n<li>1 dente de alho picado</li>\n<li>1 cebola roxa cortada em fatias finas</li>\n<li>Sal e pimenta-do-reino a gosto</li>\n<li>Folhas de manjeric&atilde;o e salsinha picada para decorar</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar. Abra os mariscos, levando os ao fogo por 2 minutos numa panela com 2 colheres (sopa) de azeite quente. Retire as conchas e reserve os moluscos e o caldo coado.</p>\n<p>Numa frigideira refogue o alho, a cebola e a pimenta com 2 colheres (sopa) de azeite. Junte o tomate-cereja e cozinhe em fogo alto por 5 minutos. Adicione o feij&atilde;o branco cozido e o caldo dos mariscos. Tempere com sal e pimenta-do-reino e acrescente a lula.</p>\n<p>Cozinhe a massa, escorra e misture ao molho de tomate, a lula e os mariscos reservados. Sirva quente, decorando o prato com folhas de manjeric&atilde;o, salsinha e pimenta-do-reino.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br />Tempo de Preparo: 25 minutos</p>',7),(10,'Pavlova de lasagna crocante','Pavlova-de-lasagna-crocante','pavlova-de-lasagna-crocante.jpg','<ul>\n<li>4 folhas de lasagna Divella</li>\n<li>40g de manteiga derretida (&frac14; de x&iacute;cara)</li>\n<li>4 colheres (ch&aacute;) de a&ccedil;&uacute;car cristal</li>\n</ul>\n<p><strong>Para o recheio:</strong></p>\n<ul>\n<li>200g de creme de leite fresco</li>\n<li>40g de a&ccedil;&uacute;car</li>\n<li>1 colher (ch&aacute;) de ess&ecirc;ncia de baunilha</li>\n<li>1 papaya sem sementes e sem casca, cortada em fatias</li>\n<li>1 kiwi sem casca cortado em fatias</li>\n<li>&frac12; manga haden sem casca, em fatias</li>\n<li>4 colheres (ch&aacute;) de polpa de maracuj&aacute;</li>\n</ul>\n<p><strong>Para povilhar:</strong></p>\n<ul>\n<li>A&ccedil;&uacute;car de confeiteiro</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar.</p>\n<p>Cozinhe as folhas de lasagna por 4 minutos, escorra-as e seque com um pano.</p>\n<p>Corte cada folha em 3 partes retangulares iguais. Pincele cada lado das massas com a manteiga, polvilhe com o a&ccedil;&uacute;car cristal e coloque-as numa assadeira untada com &oacute;leo. Asse em forno moderado (170&ordm;C), at&eacute; a massa ficar levemente dourada. Reserve.</p>\n<p>Bata o creme de leite com o a&ccedil;&uacute;car e a baunilha, at&eacute; obter o ponto de chantilly.</p>\n<p>Para a montagem da sobremesa, coloque uma folha de lasagna crocante no centro de cada prato e, por cima um pouco de chantilly. Arrume as fatias de kiwi, papaya e manga, despeje um pouco de polpa de maracuj&aacute;.</p>\n<p>Por cima, coloque outra folha de lasagna crocante e repita a opera&ccedil;&atilde;o. Finalize com uma folha de lasagna CROCANTE polvilhada com a&ccedil;&uacute;car de confeiteiro.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br />Tempo de preparo: 40 minutos</p>',8),(11,'Rigatoni com laranja e coppa','Rigatoni-com-laranja-e-coppa','rigatoni-laranja-copa.jpg','<ul>\n<li>320g de Rigatoni Divella</li>\n<li>220g de coppa cortada em tiras finas</li>\n<li>Cascas de 2 laranjas raladas</li>\n<li>120g de queijo parmes&atilde;o ralado (1 x&iacute;cara)</li>\n<li>4 ramos de alecrim</li>\n<li>4 colheres (sopa) de azeite de oliva extravirgem</li>\n<li>2 colheres (sopa) de manteiga</li>\n<li>1 dente de alho amassado</li>\n<li>Sal e pimenta-do-reino</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar.</p>\n<p>Numa frigideira, refogue no azeite o alho, o alecrim e as cascas de laranja. Adicione a manteiga e refogue por mais alguns minutos junte a coppa e tempere com sal e pimenta.</p>\n<p>Cozinhe a massa, escorra e misture ao molho preparado. Sirva quente decorada com alecrim e parmes&atilde;o.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br />Tempo de Preparo: 20 minutos</p>',9),(12,'Salada de fusilli com camarão e óleo de avelã','Salada-de-fusilli-com-camarao-e-oleo-de-avela','salada-fusilli-camarao-oleo-avela.jpg','<ul>\n<li>100g de massa tipo fusilli Divella</li>\n<li>8 camar&otilde;es-rosa grandes limpos</li>\n<li>&frac12; colher (caf&eacute;) de pimenta dedo-de-mo&ccedil;a</li>\n<li>8 colheres (sopa) de cebola picada</li>\n<li>2 colheres (ch&aacute;) de sementes de mostarda hidratadas</li>\n<li>2 colheres (ch&aacute;) de coentro picado</li>\n<li>2 colheres ( sopa) de leite de coco</li>\n<li>2 colheres 9sopa) de vinho branco seco</li>\n<li>2 colheres (sopa) de coco ralado</li>\n<li>2 (sopa) de tomate sem pele e sem sementes picado</li>\n<li>2 colheres (caf&eacute;) de &oacute;leo de avel&atilde; Beaufor</li>\n<li>Sal a gosto</li>\n</ul>\n<p><strong>Para o preparo do feij&atilde;o:</strong></p>\n<ul>\n<li>6 colheres (sopa) de feij&atilde;o-branco cozido</li>\n<li>2 colheres (sopa) de azeite de oliva extravirgem La Pastina</li>\n<li>2 colheres (sopa) de sumo de lim&atilde;o</li>\n<li>2 colheres (caf&eacute;) de &oacute;leo de palma</li>\n<li>Raspas de 2 lim&otilde;es</li>\n</ul>\n<p><strong>Para decorar:</strong></p>\n<ul>\n<li>Folhas de coentro</li>\n<li>Pimenta dedo-de-mo&ccedil;a cortada em tirinhas</li>\n<li>Folhas de minirr&uacute;cula (se desejar)</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar.</p>\n<p>Cozinho o fusilli, escorra-o e reserve.</p>\n<p>Numa frigideira, coloque o azeite, a cebola, as sementes de mostarda, a pimenta e refogue um pouco em fogo m&eacute;dio.</p>\n<p>Em seguida adicione o camar&atilde;o e refogue at&eacute; ficar dourado e cozido. Acrescente o restante dos ingredientes, deixe reduzir um pouco o caldo e reserve.</p>\n<p>Numa tigela, coloque o feij&atilde;o com o restante dos ingredientes. Junte a massa e o refogado de camar&atilde;o. Decore com as folhas de coentro, a pimenta dedo-de-mo&ccedil;a e a r&uacute;cula. Sirva em seguida.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br />Tempo de preparo: 20 minutos</p>',10),(13,'Spaghetti com salmão, dill e limão siciliano','Spaghetti-com-salmao-dill-e-limao-siciliano','spaghetti-salmao-dill-limao-siciliano.jpg','<ul>\n<li>320g de massa tipo spaghetti Divella</li>\n<li>250g de salm&atilde;o cortado em fatias</li>\n<li>300ml de creme de leite fresco (&frac12; x&iacute;cara)</li>\n<li>Raspas de 1 lim&atilde;o siciliano</li>\n<li>1 ma&ccedil;o de dill (endro) picado</li>\n<li>Sal e pimenta-do-reino a gosto</li>\n</ul>','<p>Leve ao fogo uma panela com 4 litros de &aacute;gua. Quando a &aacute;gua ferver, adicione 1 &frac12; colher (sopa) de sal e espere ferver novamente antes de colocar a massa para cozinhar.</p>\n<p>Numa panela, coloque o creme de leite e as raspas de lim&atilde;o a fogo baixo at&eacute; ferver.</p>\n<p>Adicione o salm&atilde;o e cozinhe por mais alguns minutos at&eacute; o peixe ficar cozido. Tempere com sal e pimenta-do-reino e reserve.</p>\n<p>Cozinhe a massa, escorra e misture ao molho. Junte o dill e sirva a massa<br />ainda quente. Decore o prato com pimenta-do-reino mo&iacute;da na hora e um ramo de dill.</p>\n<p>Rendimento: 4 por&ccedil;&otilde;es<br />Tempo de preparo: 30 minutos</p>',11);
/*!40000 ALTER TABLE `receitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-09-26 10:30:29
